package com.example.demo;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {
	@RequestMapping(value="/hello/{id}", method=RequestMethod.GET)
	public String hello(
			@PathVariable("id") long id,
			@RequestParam(value="filter", defaultValue="defaultFilter") String filter
	) {
		return String.format("vars: id=%s, filter=%s", id, filter);
	}

	@RequestMapping(value="/postHere/{id}", method=RequestMethod.POST)
	public PostData postHere(
			@PathVariable("id") long id,
			@RequestBody PostData p
	) {
		return p;
	}
}
